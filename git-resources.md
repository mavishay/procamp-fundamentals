
## Git Resources

|Name | Description|
|:-----|:------|
[Interactive Git Branching Learning](https://learngitbranching.js.org/) | Visual and interactive way to learn Git branching
[Learn git concepts, not commands](https://dev.to/unseenwizzard/learn-git-concepts-not-commands-4gjc) | Article on Git concepts
[Codeacademy Git Team work](https://www.codecademy.com/learn/learn-git/modules/learn-git-git-teamwork-u) | |
[Git for Computer Scientists](https://eagain.net/articles/git-for-computer-scientists/) |
