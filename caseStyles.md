
# Case Styles: And why they matter (and when)

Cover the most popular ways to combine words into a single string

**TLDR;**
Inspired by [This post](https://betterprogramming.pub/string-case-styles-camel-pascal-snake-and-kebab-case-981407998841) by [Patrick Divine
](https://pddivine.medium.com/) on medium I reflected on how this affect a DevOps Engineer's experience, it really got me wondering :thinking_face: 

* `camelCase` - commonly use
* `PascalCase` - I rarely use
* `snake_case` - commonly use
* `kebab-case` - k8s / helm use [see this](https://kubernetes.io/docs/concepts/overview/working-with-objects/names/)).

## Why Does this matter ?

Primary Goal -> Removing spaces between words, mainly because whitespaces and tabs don't `render nicely` in strings ....

In programming, we often remove the spaces between words because **_programs of different sorts reserve the space (‘ ’) character for special purposes_**. Because the **space** **character** is **reserved**, we cannot use it to represent a concept (variable / function method names) that we express in our **human language with multiple words**.

In many cases choosing `a style` is better than no style, think on actions such as [Refactoring](https://en.wikipedia.org/wiki/Code_refactoring) and how "knowing" the Case Style can help you there ;) and if all programmers had that in mind ;) ...
Java, Python, Go ... all have recommended styles, teams / projects have best-practices ;)

In some cases you will find git hooks which **brute force** these changes - respect / [`chutzpa`](https://www.merriam-webster.com/dictionary/chutzpah) - i'm not sure myself ;)

As simple shell/function example: The function `get string` is not referenced in our code as `"get string"` or it will not render (compile) ... !!!

```sh
# e.g of how we do not name our functions ...
function get string (args) = {
}
```

A typical language parser would treat each word as a separate concept. Get and String would each be treated as separate `concept`. 

So, we do something like the following:

```sh
# this works!
function getString(args) = {
}
```

Or the following:

```sh
# this works too :)
function get_string(args) = {
}
```

Now, the parser will see one concept, `getString` or `get_string`, and us programmers can easily see the representation (even if we didn't compose it originally !)

So do these things matter ? well, to the naked eye no ;) - as a troubleshooter this might be a small but yet very hand skill to master - identifying the data model - this has an impact on other markup languages such as `xml` `html` `toml` `yaml` `json` etc in some choosing a Case Style is better than not choosing one - and we all sin from time to time ;)

> In order to give Credit I suggest to get the original [story here](https://betterprogramming.pub/string-case-styles-camel-pascal-snake-and-kebab-case-981407998841) if medium blocks it the current version is as follows (without the pictures etc) + some additional reading on the subject if your really in to it ;) -> [here]() and [here]()

### Camel Case (camelCase)

Camel case combines words by capitalizing all words following the first word and removing the space, as follows:

**Raw:** `user login count`

**Camel Case:** `userLoginCount`

This is a very popular way to combine words to form a single concept. It is often used as a convention in variable declaration in many languages.

### Pascal Case (PascalCase)

Pascal case combines words by capitalizing all words (even the first word) and removing the space, as follows:

**Raw:** `user login count`

**Pascal Case:** `UserLoginCount`

This is also a very popular way to combine words to form a single concept. It is often used as a convention in declaring classes in many languages.

### Snake Case (snake_case)

Snake case combines words by replacing each space with an underscore (_) and, in the all caps version, all letters are capitalized, as follows:

**Raw:** `user login count`

**Snake Case:** `user_login_count`

**Snake Case (All Caps):** `USER_LOGIN_COUNT`

This style, when capitalized, is often used as a convention in declaring constants in many languages. When lower cased, it is used conventionally in declaring database field names.

### Kebab Case (kebab-case)

Kebab case combines words by replacing each space with a dash (-), as follows:

**Raw:** `user login count`

**Kebab Case:** `user-login-count`

This style is often used in URLs. For example, www.blog.com/cool-article-1. It is a nice, clean, human-readable way to combine the words.

## Which is best?

There is no best method of combining words. The main thing is to be consistent with the convention used, and, if you’re in a team, to come to an agreement on the convention together.

thanksForReading!
ThanksForReading!
THANKS_FOR_READING_!
thanks-for-reading-!
