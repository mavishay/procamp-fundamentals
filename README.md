# Tikal Knowledge ProCamp Fundamentals
 In order to to be ready for the camp we gathered few sources to help you be align to the ProCamp. We will at the start of the camp go over some of the sources. but It's great place to start.

 - [Get to know macOS and linux](https://gitlab.com/tikalk.com/procamp/frontend/vol2/fundamentals/-/blob/main/linux.md)
 - [The bash](https://gitlab.com/tikalk.com/procamp/frontend/vol2/fundamentals/-/blob/main/bash.md)
 - [CaseStyles And why they matter](https://gitlab.com/tikalk.com/procamp/frontend/vol2/fundamentals/-/blob/main/caseStyles.md)
 - [Git Resources](https://gitlab.com/tikalk.com/procamp/frontend/vol2/fundamentals/-/blob/main/git-resources.md)
 - [Git config basics](https://gitlab.com/tikalk.com/procamp/frontend/vol2/fundamentals/-/blob/main/git-config.md)
 - [Docker](https://gitlab.com/tikalk.com/procamp/frontend/vol2/fundamentals/-/blob/main/docker/index.md)