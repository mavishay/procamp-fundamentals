
# Docker Intro & Exercises

<img src="https://cdn4.iconfinder.com/data/icons/free-colorful-icons/360/google_docs.png" width="20px" height="20px"> Exercise [Google Doc](https://docs.google.com/document/d/182xF2Vfr0lUL8__YdOzvxLz0EsMzNCuh0BWNsVXUbOM/edit?usp=sharing) - requires access.

## What is Docker?

`Docker` is an open platform for developing, shipping, and running applications. `Docker` enables you to separate your applications from your infrastructure so you can deliver software quickly. 

With `Docker`, you can manage your infrastructure in the same ways you manage your applications. By taking advantage of `Docker`’s methodologies for shipping, testing, and deploying code quickly, you can significantly reduce the delay between writing code and running it in production.

## What can I use Docker for?

Fast, consistent delivery of your applications

Docker streamlines the development lifecycle by allowing developers to work in standardized environments using local containers which provide your applications and services. Containers are great for continuous integration and continuous delivery (CI/CD) workflows.

### 1. Consider the following example scenario:

- Developers write code locally and share their work with their colleagues using Docker containers.
- They use Docker to push their applications into a test environment and execute automated and manual tests.
- When users (qa, dev / end user) find bugs, they can fix them in the development environment and redeploy them to the test environment for testing and validation.

- When testing is complete, getting the fix to the customer is as simple as **pushing the updated image** to the `production environment`.

### 2. Responsive deployment and scaling

Docker’s container-based platform allows for highly portable workloads. Docker containers can run on a developer’s local laptop, on physical or virtual machines in a data center, on cloud providers, or in a mixture of environments.

Docker’s portability and lightweight nature also make it easy to dynamically manage workloads, scaling up or tearing down applications and services as business needs dictate, in near real time.

### 3. Running more workloads on the same hardware

Docker is lightweight and fast. It provides a viable, cost-effective alternative to hypervisor-based virtual machines, so you can use more of your compute capacity to achieve your business goals. Docker is perfect for high density environments and for small and medium deployments where you need to do more with fewer resources.

## Docker architecture🔗

![HLA](https://docs.docker.com/engine/images/architecture.svg)

## > The Docker daemon

The Docker daemon (dockerd) listens for Docker API requests and manages `Docker objects` such as **images**, **containers**, **networks**, and **volumes**. 
A daemon can also communicate with other daemons to manage Docker services.

## > The Docker client

The `Docker` client (docker) is the primary way that many Docker users interact with Docker. When you use commands such as `docker run`, the client **sends these commands to dockerd**, which carries them out. 

The `docker command` uses the Docker API. => The Docker client can `communicate with more than one daemon`.

## > Docker registries

A **Docker registry** stores `Docker images`. **Docker Hub** is a public *registry that anyone can use,* and Docker is configured to look for images on Docker Hub by default. 
You can even run your own private registry, from a simple container to a full blown solution suach as [harbor](https://goharbor.io/)

When you use the `docker pull` or `docker run` commands, the required images are pulled from your configured registry. When you use the `docker push` command, your image is pushed to your configured registry.


## > Docker objects

When you use Docker, you are creating and using images, containers, networks, volumes, plugins, and other objects. This section is a brief overview of some of those objects.

Images
An image is a read-only template with instructions for creating a Docker container. Often, an image is based on another image, with some additional customization. For example, you may build an image which is based on the ubuntu image, but installs the Apache web server and your application, as well as the configuration details needed to make your application run.

You might create your own images or you might only use those created by others and published in a registry. To build your own image, you create a Dockerfile with a simple syntax for defining the steps needed to create the image and run it. Each instruction in a Dockerfile creates a layer in the image. When you change the Dockerfile and rebuild the image, only those layers which have changed are rebuilt. This is part of what makes images so lightweight, small, and fast, when compared to other virtualization technologies.

Containers
A container is a runnable instance of an image. You can create, start, stop, move, or delete a container using the Docker API or CLI. You can connect a container to one or more networks, attach storage to it, or even create a new image based on its current state.

By default, a container is relatively well isolated from other containers and its host machine. You can control how isolated a container’s network, storage, or other underlying subsystems are from other containers or from the host machine.

A container is defined by its image as well as any configuration options you provide to it when you create or start it. When a container is removed, any changes to its state that are not stored in persistent storage disappear.

Example docker run command
The following command runs an ubuntu container, attaches interactively to your local command-line session, and runs /bin/bash.

```sh
$ docker run -i -t ubuntu /bin/bash
```

When you run this command, the following happens (assuming you are using the default registry configuration):

1. If you do not have the ubuntu image locally, Docker pulls it from your configured registry, as though you had run docker pull ubuntu manually.

2. Docker creates a new container, as though you had run a docker container create command manually.

3. Docker allocates a read-write filesystem to the container, as its final layer. This allows a running container to create or modify files and directories in its local filesystem.

4. Docker creates a `network interface` to connect the container to the default network, since you did not specify any networking options. 
This includes assigning an IP address to the container. 
By default, containers can connect to external networks using the host machine’s network connection.

5. Docker starts the container and executes `/bin/bash`. **Considering the container is running interactively and attached to your terminal** (due to the -`i` and `-t` flags), you can provide input using your keyboard while the output is logged to your terminal.

When you type exit to terminate the `/bin/bash` command, the container stops but is not removed. 
You can start it again or remove it.

In order to run a daemon mode using `-d` will send the container into the bacground working as a service.

These are the bare basics a more through tutorial can be found online a few notable resources below:


## Katacoda
- [Paly with Docker](https://labs.play-with-docker.com/)
- [Katacode Docker Online excersizes](https://www.katacoda.com/courses/docker)

## A great 3 hour tutorial

<iframe width="560" height="315" src="https://www.youtube.com/embed/3c-iBn73dDE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Littirally every project out there has a `Dockerfile` - the standard way of shipping applications 

![img](https://secure.meetupstatic.com/photos/event/9/1/e/2/600_481297346.jpeg)
