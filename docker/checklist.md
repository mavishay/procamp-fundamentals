
# Docker Checklist

## Tools

Name | Description
:------|:------:
[dive](https://github.com/wagoodman/dive) | "A tool for exploring a docker image, layer content, ..."
[trivy](https://github.com/aquasecurity/trivy) | "A Simple and Comprehensive Vulnerability Scanner for Containers and other Artifacts, Suitable for CI."

## Docker - Resources

Name | Description
:------|:------:
[Docker Compose](https://docs.docker.com/compose/overview) | Multi-container applications

## Docker - Checklist

- [ ] **Images**
  - [ ] tag
  - [ ] `docker images` command

- [ ] **Running containers**
  - [ ] `docker run` command
  - [ ] `-ti` arguments for interactive terminal

- [ ] **List containers**
  - [ ] `docker ps` command
  - [ ] `-a` argument for stopped
  - [ ] `-l` argument for last stopped

- [ ] **Commit changes**
  - [ ] `docker commit` command
  - [ ] `docker tag` command

